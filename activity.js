//insertOne method: insert the following details

db.rooms.insert({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})

//insertMany method: with the following details

db.rooms.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A simple room fit for small family going on a vacation.",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway.",
		rooms_available: 15,
		isAvailable: false
	}
])

//finOne method: name: double

db.rooms.findOne({name: "double"})

//updateOne method: available rooms = 0

db.rooms.updateOne(
	{_id: ObjectId("628785cdbe49cee9d9f67deb")},
	{
		$set: {
			name: "queen",
			accomodates: 4,
			price: 4000,
			description: "A room with a queen sized bed perfect for a simple getaway.",
			rooms_available: 0,
			isAvailable: false
		}
	}
)

//deleteMany method: rooms that have 0 availability

db.rooms.deleteMany({
	rooms_available	: 0
})